import {Router} from "express";
import path from "path";
import {HTML_FILES_PATH} from "../config.js";
import {texts} from "../data.js";

const router = Router();

router
    .get("/", (req, res) => {
        const page = path.join(HTML_FILES_PATH, "game.html");
        res.sendFile(page);
    })
    .get("/texts/:id", (req, res) => {
        const textId = req.params.id
        if (textId >= texts.length) {
            res.statusCode = 404
            return
        }
        res.send(JSON.stringify({
            text: texts[req.params.id]
        }))

    })

export default router;
