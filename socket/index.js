import game from "./gameRooms.js";

export default io => {
    game(io.of("/game"));
};

