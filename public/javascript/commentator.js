import {getRandomInt} from "./helper.mjs";

export const INTRODUCE_SELF = 'INTRODUCE_SELF';
export const INTRODUCE_RACERS = 'INTRODUCE_RACERS';
export const USER_THIRTY_CHARS_MSG = 'USER_THIRTY_CHARS_MSG';
export const USER_FINISHED = 'USER_FINISHED';
export const GAME_FINISHED = 'GAME_FINISHED';


const commentatorMsgElem = document.getElementById('commentator-msg')

let roomInProgress;

//Pattern Facade - technically,
export const updateCommentator = (event, room, {username, progress} = {}) => {
    roomInProgress = room;
    setInterval(() => {
        commentatorMsgElem.innerText = roomStandingsMsg(roomInProgress)

    }, 30000)
    switch (event) {
        case INTRODUCE_SELF:
            commentatorMsgElem.innerText = 'Hi there'
            return;
        case  INTRODUCE_RACERS:
            const descriptions = roomInProgress.users.map(user => `${user.name} is in his ${cars[getRandomInt(cars.length)]} `)
            commentatorMsgElem.innerText = descriptions.join('\n')
            return;
        case  USER_THIRTY_CHARS_MSG:
            commentatorMsgElem.innerText = `${username} is getting so close, GO GO!`
            return;
        case  USER_FINISHED:
            console.log('USER_FINISHED', room)
            commentatorMsgElem.innerText = `${username} crossed the finish line!!!${exclamation[getRandomInt(exclamation.length)]}`
            return;

        case  GAME_FINISHED:
            commentatorMsgElem.innerText = finishMsg(room)

    }

}


const roomStandingsMsg = room => {
    const progressInfo = room.users.map(user =>
        ({
            name: user.name,
            progress: user.progress

        }))
    progressInfo.sort((a, b) => a.progress > b.progress ? -1 : 1)
    return progressInfo.map(info => {
        if (info.progress === 100) {
            return `${info.name}'s already finished, give him congrats! ${exclamation[getRandomInt(exclamation.length)]}\n`
        }
        return `${info.name}'s progress is ${info.progress} so far\n`
    }).join(' ')

}
const finishMsg = room => {
    const finishMsg = room.users.map(user =>
        ({
            name: user.name,
            finishedAt: user.finishedAt

        }))
    finishMsg.sort((a, b) => a.finishedAt < b.finishedAt ? -1 : 1)
    return 'That was hell of a game!\n' + finishMsg.map((info, index) => {
        if (index < 3)
            return `${info.name}'s came in ${index} place\n`
        return ''
    })

}

const cars = ['Aston Martin Vantage', 'Audi R8', 'Chevrolet Corvette',
    'Mercedes SLS-Class', 'Nissan GT-R', 'Ford Mustang', 'Ford GT', 'Porsche 911', 'Aston Martin Vulcan']

const exclamation = ['Wow', 'Wonderful', 'Amaaaaaazing', 'Shocking, right?', 'Such speed', 'Look at that!']
