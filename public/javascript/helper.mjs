export const createElement = ({tagName, className, attributes = {}}) => {
    const element = document.createElement(tagName);

    if (className) {
        addClass(element, className);
    }

    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
};

export const addClass = (element, className) => {
    const classNames = formatClassNames(className);
    element.classList.add(...classNames);
};

export const removeClass = (element, className) => {
    const classNames = formatClassNames(className);
    element.classList.remove(...classNames);
};

export const formatClassNames = className => className.split(" ").filter(Boolean);

export const getSecondsRemaining = (start, end) => {
    let total = new Date(end).getTime() - new Date(start).getTime();
    let seconds = Math.floor((total / 1000) % 60);

    return {
        total,
        seconds
    };
}

function getTimeRemaining(endtime) {
    let total = new Date(endtime).getTime() - new Date().getTime();
    let seconds = Math.floor((total / 1000) % 60);

    return {
        total,
        seconds
    };
}

export function initializeClock(id, endtime, callback) {
    const clock = document.getElementById(id);
    const timeinterval = setInterval(() => {
        const t = getTimeRemaining(endtime);
        clock.innerHTML = 'seconds: ' + t.seconds;
        if (t.total <= 1) {
            clearInterval(timeinterval);
            callback()
        }
    }, 1000);
    return timeinterval
}

export function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

export async function fetchText(textId) {

    let response = await fetch(`/game/texts/${textId}`);

    if (response.status === 200) {
        return await response.json()
    }
}
