import {addClass, createElement, fetchText, initializeClock, removeClass} from "./helper.mjs";
import {createRacerElement} from "./raceElementCreators.js";
import {startRace} from "./race.mjs";
import {GAME_FINISHED, updateCommentator, USER_FINISHED, USER_THIRTY_CHARS_MSG} from "./commentator.js";
import {INTRODUCE_RACERS, INTRODUCE_SELF} from "./commentator";
import {createRoomElement} from "./roomElemCreators.js";

const username = sessionStorage.getItem("username");

if (!username) {
    window.location.replace("/login");
}
const socket = io("http://localhost:3002/game", {query: {username}});


const roomContainer = document.getElementById("rooms-container");
const backToRoomsBtn = document.getElementById("back-btn");
const submitRoomButton = document.getElementById("submit-room-btn");
const raceField = document.getElementById("race-field");

const roomInfoTitle = document.getElementById("room-info-title");
const racersContainer = document.getElementById("racers-container");


const createRoomButton = document.getElementById("create-room-btn");
const createRoomPrompt = document.getElementById("create-room-prompt");

const onClickCreateRoomButton = () => {
    if (createRoomButton.innerText === "Cancel") {
        addClass(createRoomPrompt, "display-none")
        addClass(submitRoomButton, "display-none")
        createRoomButton.innerText = "Create Room"
    } else if (createRoomButton.innerText === "Create Room") {
        removeClass(createRoomPrompt, "display-none")
        removeClass(submitRoomButton, "display-none")
        createRoomButton.innerText = "Cancel"

    }
}

const onClickSubmitRoomButton = () => {
    if (createRoomPrompt.value === '') {
        return
    }
    socket.emit("CREATE_ROOM", createRoomPrompt.value);
    addClass(createRoomPrompt, "display-none")
    addClass(submitRoomButton, "display-none")
    createRoomButton.innerText = "Create Room"
    createRoomPrompt.value = ''
};

createRoomButton.addEventListener("click", onClickCreateRoomButton);
submitRoomButton.addEventListener("click", onClickSubmitRoomButton);
backToRoomsBtn.addEventListener("click", () => {
    socket.emit("LEAVE_ROOM", {
        roomId: document.getElementById("room-info-title").innerText, username
    })

});


const updateRooms = rooms => {
    roomContainer.innerHTML = '';
    rooms.map(room => roomContainer.appendChild(createRoomElement(room)))

};

let removeEventListeners;
let gameInterval;

const initRace = (text, room) => {
    raceField.innerHTML = '';
    raceField.appendChild(createElement({
        tagName: "span",
        attributes: {id: 'game-timer-clock'}
    }))
    raceField.appendChild(createElement({tagName: 'br'}))

    removeEventListeners = startRace(username, text, updateUserProgress(room,
        room.users.filter(user => user.name === username)))

    gameInterval = initializeClock(
        'game-timer-clock',
        room.game.duration, () => {
            removeEventListeners()
        }
    )

}

const updateUserProgress = (user) => (progress, charsLeft) => {
    const state = {roomId: roomInProgress.id, username, ready: user.ready, progress: progress,}
    if (progress === 100) {
        state.finishedAt = new Date().getTime()
        updateCommentator(USER_FINISHED, roomInProgress, {username, progress})
    }
    if (charsLeft === 30) {
        updateCommentator(USER_THIRTY_CHARS_MSG, roomInProgress, user.name)
    }
    socket.emit("USER_NEW_STATE", state)
}


const prepareRoomBeforeRace = room => {
    addClass(document.getElementById("rooms-page"), "display-none")
    removeClass(document.getElementById("game-page"), "display-none")

    racersContainer.innerHTML = '';
    raceField.innerHTML = '';
    const readyBtn = createElement({
        tagName: "button",
        attributes: {id: "ready-btn"}
    })
    roomInfoTitle.innerText = room.id;
    room.users.map(user => {
        const racer = createRacerElement(user, user.name === username)
        if (user.name === username) {
            readyBtn.innerText = user.ready ? "Not ready" : "Ready"
            readyBtn.addEventListener("click", () => {
                socket.emit("USER_NEW_STATE", {roomId: room.id, username, isReady: !user.ready})
            });
        }
        racersContainer.appendChild(racer)
    });
    raceField.appendChild(readyBtn)
};


let textLength;
let roomInProgress;

const updateRoomInProgress = room => {

    let areAllFinished = true;
    room.users.map(user => {
        const userProgress = document.getElementById(`${user.name}-progress-bar`)
        userProgress.style.width = user.progress + '%';
        if (user.progress === 100) {
            addClass(userProgress, 'progress-bar-full')
        } else {
            areAllFinished = false
        }
    });

    if (areAllFinished || room.state === "ROOM_FINISHED") {
        updateCommentator(GAME_FINISHED, room)

        document.getElementById('game-timer-clock').innerHTML = ''
        removeEventListeners()
        clearInterval(gameInterval)
        socket.emit("GAME_FINISHED", room.id)
        prepareRoomBeforeRace(room)

    }
}

async function updateRoomReady(room) {
    socket.emit("ROOM_IN_PROGRESS", room.id)
    raceField.innerHTML = '';
    raceField.appendChild(createElement({
        tagName: "span",
        attributes: {id: 'start-timer-clock'}
    }))

    raceField.appendChild(createElement({tagName: 'br'}))

    removeClass(backToRoomsBtn, 'display-none')
    const text = await fetchText(room.game.textId)
    textLength = text.text.length

    updateCommentator(INTRODUCE_SELF, room)
    initializeClock(
        'start-timer-clock',
        room.game.startAt, () => {
            initRace(text.text, room)
            setTimeout(() => updateCommentator(INTRODUCE_RACERS, room), 2000)


        }
    )
}

async function joinRoom(room) {
    roomInProgress = room
    prepareRoomBeforeRace(room)
}

async function updateRoom(room) {
    roomInProgress = room
    if (room.state === "ROOM_IN_PROGRESS") {
        updateRoomInProgress(room)
        return

    }

    if (room.state === "ROOM_READY") {
        await updateRoomReady(room)
    }

}

socket.on("UPDATE_ROOMS", updateRooms);
socket.on("CREATE_ROOM_DONE", joinRoom);
socket.on("JOIN_ROOM_DONE", joinRoom);
socket.on("NEW_USER_FORBIDDEN", () => {
    alert('such nickname is already in use')
    window.location.replace("/login");
    sessionStorage.removeItem('username')
});
socket.on("CREATE_ROOM_FORBIDDEN", () => {
    alert('room with such title is already in use')
});
socket.on("UPDATE_ROOM_STATE", room => {
    if (roomInfoTitle.innerText === room.id) {
        updateRoom(room)
    }
});

